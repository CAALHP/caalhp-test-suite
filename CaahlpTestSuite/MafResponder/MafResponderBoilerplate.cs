﻿using System.AddIn;
using MafResponder;
using Plugins.ServicePluginAdapter;
using ResponderLib;

namespace MafResponder
{
    [AddIn("MafResponder", Version = "1.0.0.0")]
    public class MafResponderBoilerplate : ServiceViewPluginAdapter
    {
        public MafResponderBoilerplate()
        {
            Service = new Responder("MafResponder", new MafResponderEventHandler());
        }
    }
}
