﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using CAALHP.Contracts;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using ResponderLib;
using TestSuite.Events;

namespace MafResponder
{
    public class MafResponderEventHandler : IResponderEventHandler
    {
        public IServiceHostCAALHPContract Host { get; set; }
        public int ProcessID { get; set; }

        private const string Libname = "TestSuite";
        private const string _name = "MafResponder";

        public void HandleEvent(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            Handle(obj);
        }

        private void Handle(TestMafRequestEvent e)
        {
            var request = new TestMafResponseEvent
            {
                Index = e.Index,
                CallerName = _name,
                CallerProcessId = ProcessID,
            };
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, request, Libname);

            Host.Host.ReportEvent(serializedCommand);
        }

        private void Handle(TestMafResponseEvent e)
        {
            //todo remove this function
        }

        public void SubscribeToEvents()
        {
            Console.WriteLine("Subscribing to events..");
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(TestMafRequestEvent), Libname), ProcessID);
        }
    }
}
