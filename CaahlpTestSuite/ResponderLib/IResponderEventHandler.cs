﻿using System.Collections.Generic;
using CAALHP.Contracts;
using TestSuite.Events;

namespace ResponderLib
{
    public interface IResponderEventHandler
    {
        void HandleEvent(KeyValuePair<string, string> notification);
        IServiceHostCAALHPContract Host { get; set; }
        int ProcessID { get; set; }
        void SubscribeToEvents();
    }
}
