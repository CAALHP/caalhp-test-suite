﻿using System;
using System.Collections.Generic;
using CAALHP.Contracts;

namespace ResponderLib
{
    public class Responder : IServiceCAALHPContract
    {
        private readonly string _name;
        private IServiceHostCAALHPContract _host { get; set; }
        private int _processId;
        private const string Libname = "TestSuite";
        private readonly IResponderEventHandler _eventHandler;

        public Responder(string name, IResponderEventHandler eventHandler)
        {
            _name = name;
            _eventHandler = eventHandler;
        }

        public string GetName()
        {
            return _name;
        }

        public void Stop()
        {
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            _eventHandler.HandleEvent(notification);
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            _eventHandler.Host = _host;
            _eventHandler.ProcessID = processId;
            _eventHandler.SubscribeToEvents();
        }

        public void Start()
        {
        }
    }
}