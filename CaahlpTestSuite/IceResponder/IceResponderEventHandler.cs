﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Contracts;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using ResponderLib;
using TestSuite.Events;

namespace IceResponder
{
    class IceResponderEventHandler : IResponderEventHandler
    {
        public IServiceHostCAALHPContract Host { get; set; }
        public int ProcessID { get; set; }

        private const string Libname = "TestSuite";
        private const string _name = "IceResponder";

        public void HandleEvent(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            Handle(obj);
        }

        private void Handle(TestIceRequestEvent e)
        {
            var request = new TestIceResponseEvent { Index = e.Index };
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, request, Libname);
            Host.Host.ReportEvent(serializedCommand);
        }

        private void Handle(TestMafRequestEvent e)
        {
            // Gør det samme som ICE
        }

        public void SubscribeToEvents()
        {
            Console.WriteLine("Subscribing to events..");
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(TestIceRequestEvent), Libname), ProcessID);
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(TestMafRequestEvent), Libname), ProcessID);
        }
    }
}
