﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.SOA.ICE.ClientAdapters;
using ResponderLib;

namespace IceResponder
{
    class Program
    {
        static void Main(string[] args)
        {

            //we assume the host is running on localhost:
            const string endpoint = "localhost";
            try
            {
                //creating the ServiceAdapter - which also handles connection to the endpoint. 
                //The second parameter is a reference to the implementation of an IServiceCAALHPContract.

                var reqImp = new Responder("IceResponder", new IceResponderEventHandler());
                var adapter = new ServiceAdapter(endpoint, reqImp);
                Console.WriteLine("IceResponder running!");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                //Connection to host probably failed
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }

        }
    }
}
