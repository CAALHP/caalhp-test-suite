﻿namespace CAALHP.Utils.Helpers.Serialization
{
    public enum SerializationType
    {
        Json, String, Xml
    }
}
