﻿using System;
using System.Collections.Generic;
using CAALHP.Contracts;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using TestSuite.Events;

namespace RequesterLib
{
    public class Requester : IServiceCAALHPContract
    {
        private readonly string _name;
        private IServiceHostCAALHPContract _host { get; set; }
        private int _processId;
        private const string Libname = "TestSuite";
        private readonly IRequesterEventHandler _eventHandler;

        public Requester(string name, IRequesterEventHandler eventHandler)
        {
            _name = name;
            _eventHandler = eventHandler;
        }

        public string GetName()
        {
            return _name;
        }

        public void Stop()
        {
        }

        public void Notify(KeyValuePair<string, string> notification)
        {   
            _eventHandler.HandleEvent(notification);
        }


        public void RequestMAFEvent()
        {
            var request = new TestMafRequestEvent
            {
                CallerName = _name
            };
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, request, Libname);
            _host.Host.ReportEvent(serializedCommand);
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            _eventHandler.Host = _host;
            _eventHandler.ProcessID = processId;
            _eventHandler.SubscribeToEvents();
        }

        public void Start()
        {
        }
    }
}