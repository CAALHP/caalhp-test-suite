﻿using System.Collections.Generic;
using CAALHP.Contracts;
using TestSuite.Events;

namespace RequesterLib
{
    public interface IRequesterEventHandler
    {
        void HandleEvent(KeyValuePair<string, string> notification);
        IServiceHostCAALHPContract Host { get; set; }
        int ProcessID { get; set; }
        void SubscribeToEvents();
    }
}
