﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using RequesterLib;
using TestSuite.Events;

namespace MafRequester
{
    public class MafRequesterEventHandler : IRequesterEventHandler
    {
        public IServiceHostCAALHPContract Host { get; set; }
        public int ProcessID { get; set; }

        private const string Libname = "TestSuite";
        private const string _name = "MafRequester";
        private Dictionary<int, TestMafRequestEvent> _requesterDictionary;
        private DateTime _timeBefore;
        private DateTime _timeAfter;
        private int _cycles;
        private int _mafCounter;

        private object _lock = new Object();
        private object _writelock = new object();

        public void HandleEvent(KeyValuePair<string, string> notification)
        {
            Task.Run(() =>
            {
                var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
                dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
                Handle(obj);
            });
        }

        private void Handle(TestMafResponseEvent o)
        {
            bool wasremoved = false;

            lock (_lock)
            {
                _mafCounter--;

                if (_mafCounter == 0)
                {
                    _timeAfter = DateTime.Now;

                    var ticks = _timeAfter.Ticks - _timeBefore.Ticks;
                    long microseconds = ticks / (TimeSpan.TicksPerMillisecond / 1000);
                    var average = microseconds / _cycles;

                    ReportResults(_cycles, "Maf2MafTest", ticks, average);
                }

                //lock (_writelock)
                //{
                //    WriteToFile(_mafCounter.ToString());
                //}
            }

        }

        private void WriteToFile(string str)
        {
            // Compose a string that consists of three lines.
            string lines = str;
            const string filename = "test.txt";

            using (var file = new StreamWriter(filename, true))
            {
                file.WriteLine(lines);
            }

        }

        private void Handle(StartMaf2IceTestEvent o)
        {
            //StartMaf2IceTest(o.Cycles);
        }

        private void Handle(StartMafMethodSpamTest o)
        {
            StartMafMethodSpamTest(o.Cycles);
        }

        private void Handle(Event e)
        {
            lock (_writelock)
            {
                WriteToFile(DateTime.Now + ": Fejlevent!");
            }
        }

        private void Handle(StartMaf2MafTestEvent o)
        {
            StartMaf2MafTest(o.Cycles);
        }

        public void SubscribeToEvents()
        {
            Console.WriteLine("Subscribing to events..");
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(TestMafResponseEvent), Libname), ProcessID);
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(TestIceResponseEvent), Libname), ProcessID);
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(StartMaf2IceTestEvent), Libname), ProcessID);
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(StartMaf2MafTestEvent), Libname), ProcessID);
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(StartMafMethodSpamTest), Libname), ProcessID);
        }

        public void RequestICEEvent(TestIceRequestEvent request)
        {
            Task.Run(() =>
            {
                var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, request, Libname);
                Host.Host.ReportEvent(serializedCommand);
            });
        }

        public void RequestMafEvent(TestMafRequestEvent request)
        {
            Task.Run(() =>
            {
                var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, request, Libname);
                Host.Host.ReportEvent(serializedCommand);
            });
        }

        //private void StartMaf2IceTest(int cycles)
        //{
        //    _cycles = cycles;
        //    Console.WriteLine("Starting Maf2Ice test...");

        //    Host.ReportEvent(EventHelper.CreateEvent(SerializationType.Json, new InitializedEvent()));

        //    _requesterDictionary = new Dictionary<int, TestIceRequestEvent>();

        //    for (int i = 0; i < cycles; i++)
        //    {
        //        _requesterDictionary.Add(i, new TestIceRequestEvent() { CallerName = _name, Index = i });
        //    }

        //    _timeBefore = DateTime.Now;

        //    for (int i = 0; i < cycles; i++)
        //    {
        //        RequestICEEvent(_requesterDictionary[i]);
        //    }
        //}

        private void StartMaf2MafTest(int cycles)
        {
            _cycles = cycles;
            _mafCounter = cycles;
            Console.WriteLine("Starting Maf2Maf test...");

            Host.Host.ReportEvent(EventHelper.CreateEvent(SerializationType.Json, new InitializedEvent()));
            _timeBefore = DateTime.Now;

            for (int i = 0; i < cycles; i++)
            {
                RequestMafEvent(new TestMafRequestEvent() { CallerName = _name, CallerProcessId = ProcessID });
            }
        }

        private void StartMafMethodSpamTest(int cycles)
        {
            _cycles = cycles;
            Console.WriteLine("Starting MafMethodSpam test...");

            _timeBefore = DateTime.Now;

            for (int i = 0; i < cycles; i++)
            {
                Host.GetListOfInstalledApps();
            }

            _timeAfter = DateTime.Now;

            var ticks = _timeAfter.Ticks - _timeBefore.Ticks;
            long microseconds = ticks / (TimeSpan.TicksPerMillisecond / 1000);
            var average = microseconds / _cycles;

            ReportResults(_cycles, "Maf2MafTest", ticks, average);
        }

        public void ReportResults(int cycles, string name, long durationInTicks, long averageTimePerTick)
        {
            Console.WriteLine("Reporting results for test " + name + ".");

            var request = new TestCompletedEvent
            {
                CallerName = _name,
                Cycles = cycles,
                Name = name,
                DurationInTicks = durationInTicks,
                AverageTimePerEvent = averageTimePerTick
            };

            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, request, Libname);
            Host.Host.ReportEvent(serializedCommand);
            Console.WriteLine("Test completed.");
        }
    }
}
