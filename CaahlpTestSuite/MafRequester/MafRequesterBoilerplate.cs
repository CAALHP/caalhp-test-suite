﻿using System.AddIn;
using Plugins.ServicePluginAdapter;
using RequesterLib;

namespace MafRequester
{
    [AddIn("MafRequester", Version = "1.0.0.0")]
    public class MafRequesterBoilerplate : ServiceViewPluginAdapter
    {
        public MafRequesterBoilerplate()
        {
            Service = new Requester("MafRequester", new MafRequesterEventHandler());
        }
    }
}
