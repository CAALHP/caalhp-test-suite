﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using CAALHP.Contracts;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using TestSuite.Events;

namespace TestConsole
{
    public class TestConsoleImplementation : IServiceCAALHPContract
    {
        private readonly string _name;
        private IServiceHostCAALHPContract _host { get; set; }
        private int _processId;
        private const string Libname = "TestSuite";

        public TestConsoleImplementation(string name)
        {
            _name = name;
        }

        public string GetName()
        {
            return _name;
        }

        public void Stop()
        {
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        public void StartIce2IceTest(int cycles)
        {
            Console.WriteLine("Request for Ice2Ice test start sent!");
            var request = new StartIce2IceTestEvent();
            request.CallerName = _name;
            request.Cycles = cycles;

            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, request, Libname);
            _host.Host.ReportEvent(serializedCommand);
        }

        public void StartMafMethodSpamTest(int cycles)
        {
            Console.WriteLine("Request for MafMethodSpam test start sent!");
            var request = new StartMafMethodSpamTest();
            request.CallerName = _name;
            request.Cycles = cycles;

            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, request, Libname);
            _host.Host.ReportEvent(serializedCommand);

        }

        public void StartIce2MafTest(int cycles)
        {
            Console.WriteLine("Request for Ice2Maf test start sent!");
            var request = new StartIce2MafTestEvent();
            request.CallerName = _name;
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, request, Libname);
            _host.Host.ReportEvent(serializedCommand);
        }

        public void StartMaf2MafTest(int cycles)
        {
            Console.WriteLine("Request for Maf2Maf test start sent!");
            var request = new StartMaf2MafTestEvent();
            request.CallerName = _name;
            request.CallerProcessId = _processId;
            request.Cycles = cycles;
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, request, Libname);
            _host.Host.ReportEvent(serializedCommand);
        }

        public void StartMaf2IceTest(int cycles)
        {
            Console.WriteLine("Request for Maf2Ice test start sent!");
            var request = new StartMaf2IceTestEvent();
            request.CallerName = _name;
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, request, Libname);
            _host.Host.ReportEvent(serializedCommand);
        }

        public void HandleEvent(TestIceResponseEvent e)
        {
        }

        public void HandleEvent(TestCompletedEvent e)
        {
            var microsecs = e.DurationInTicks / (TimeSpan.TicksPerMillisecond / 1000);

            Console.WriteLine(e.Name + " test completed.");
            Console.WriteLine("Events sent: " + e.Cycles);
            Console.WriteLine("Test duration: " + microsecs + " microsecs.");
            Console.WriteLine("Average time per event: " + e.AverageTimePerEvent + " microseconds.\n");
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(TestCompletedEvent), Libname), _processId);
        }

        public void Start()
        {
        }

    }
}
