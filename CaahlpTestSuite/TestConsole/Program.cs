﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.SOA.ICE.ClientAdapters;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {

            //we assume the host is running on localhost:
            const string endpoint = "localhost";
            try
            {
                //creating the ServiceAdapter - which also handles connection to the endpoint. 
                //The second parameter is a reference to the implementation of an IServiceCAALHPContract.

                var reqImp = new TestConsoleImplementation("TestConsole");
                var adapter = new ServiceAdapter(endpoint, reqImp);

                //var continueRunning = true;
                Console.WriteLine("TestConsole service running!");
                Console.WriteLine("Enter how many eventcycles to test:");
                string input = Console.ReadLine();


                int value;

                if (int.TryParse(input, out value)) // Try to parse the string as an integer
                {
                    Console.WriteLine("Sending request for test with " + value + " cycles");

                    reqImp.StartIce2IceTest(value);
                }
                else
                {
                    Console.WriteLine("Not an integer!");
                }

                Console.ReadLine();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
