﻿using System;
using CAALHP.SOA.ICE.ClientAdapters;
using RequesterLib;
using Exception = System.Exception;

namespace IceRequester
{
    class Program
    {
        static void Main(string[] args)
        {

            //we assume the host is running on localhost:
            const string endpoint = "localhost";
            try
            {
                //creating the ServiceAdapter - which also handles connection to the endpoint. 
                //The second parameter is a reference to the implementation of an IServiceCAALHPContract.

                var reqImp = new Requester("IceRequester", new IceEventHandler());
                var adapter = new ServiceAdapter(endpoint, reqImp);
                Console.WriteLine("IceRequester running!\nPress Enter exit.");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                //Connection to host probably failed
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }

        }
    }
}
