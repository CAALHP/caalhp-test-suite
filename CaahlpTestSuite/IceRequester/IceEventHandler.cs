﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using Ice;
using Newtonsoft.Json;
using RequesterLib;
using TestSuite.Events;
using JsonSerializer = CAALHP.Utils.Helpers.Serialization.JSON.JsonSerializer;

namespace IceRequester
{
    public class IceEventHandler : IRequesterEventHandler
    {
        public IServiceHostCAALHPContract Host { get; set; }
        public int ProcessID { get; set; }

        private const string Libname = "TestSuite";
        private const string _name = "IceRequester";
        private Dictionary<int, TestIceRequestEvent> _requesterDictionary;
        private DateTime _timeBefore;
        private DateTime _timeAfter;
        private int _cycles;

        public void HandleEvent(KeyValuePair<string, string> notification)
        {
            Task.Run(() =>
            {
                var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
                dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
                Handle(obj);
            });
        }

        private void Handle(TestIceResponseEvent o)
        {
            lock (_requesterDictionary)
            {
                _requesterDictionary.Remove(o.Index);
                if (_requesterDictionary.Count == 0)
                {
                    _timeAfter = DateTime.Now;

                    var ticks = _timeAfter.Ticks - _timeBefore.Ticks;
                    long microseconds = ticks / (TimeSpan.TicksPerMillisecond / 1000);
                    var average = microseconds / _cycles;

                    ReportResults(_cycles, "Ice2IceTest", ticks, average);
                }
            }
        }

        private void Handle(StartIce2IceTestEvent o)
        {
            StartIce2IceTest(o.Cycles);
        }

        private void Handle(StartIce2MafTestEvent o)
        {
            // Do something
        }

        public void SubscribeToEvents()
        {
            Console.WriteLine("Subscribing to events..");
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(TestIceResponseEvent), Libname), ProcessID);
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(StartIce2IceTestEvent), Libname), ProcessID);
            Host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(StartIce2MafTestEvent), Libname), ProcessID);
        }

        public void RequestICEEvent(TestIceRequestEvent request)
        {
            //var request = new TestIceRequestEvent();
            Task.Run(() =>
            {
                var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, request, Libname);
                Host.Host.ReportEvent(serializedCommand);
            });
        }

        private void StartIce2IceTest(int cycles)
        {
            _cycles = cycles;
            Console.WriteLine("Starting Ice2Ice test...");
            Host.Host.ReportEvent(EventHelper.CreateEvent(SerializationType.Json, new InitializedEvent()));
            _requesterDictionary = new Dictionary<int, TestIceRequestEvent>();

            for (int i = 0; i < cycles; i++)
            {
                _requesterDictionary.Add(i, new TestIceRequestEvent() { CallerName = _name, Index = i });
            }

            _timeBefore = DateTime.Now;

            for (int i = 0; i < cycles; i++)
            {
                RequestICEEvent(_requesterDictionary[i]);
            }
        }

        public void ReportResults(int cycles, string name, long durationInTicks, long averageTimePerTick)
        {
            Console.WriteLine("Reporting results for test " + name + ".");

            var request = new TestCompletedEvent
            {
                CallerName = _name,
                Cycles = cycles,
                Name = name,
                DurationInTicks = durationInTicks,
                AverageTimePerEvent = averageTimePerTick
            };

            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, request, Libname);
            Host.Host.ReportEvent(serializedCommand);
            Console.WriteLine("Test completed.");
        }
    }
}