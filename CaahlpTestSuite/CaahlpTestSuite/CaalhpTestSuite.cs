﻿using System;
using System.Collections.Generic;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;

namespace TestSuite                               
{
    public class CaalhpTestSuite : IAppCAALHPContract
    {
        private IAppHostCAALHPContract _host;
        private int _processId;
        private const string AppName = "TestSuite";

        public CaalhpTestSuite()
        {
            
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(ShowAppEvent e)
        {
            if (e.AppName.Equals(AppName))
            {
                Show();
            }
        }

        public string GetName()
        {
            return "Caalhp Test Suite";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);

        }

        public void Show()
        {
            
        }
    }
}