﻿using CAALHP.Events;

namespace TestSuite.Events
{
    public class StartMaf2MafTestEvent : Event
    {
        public int Cycles { get; set; }
    }
}
