﻿using CAALHP.Events;

namespace TestSuite.Events
{
    public class StartIce2MafTestEvent : Event
    {
        public int Cycles { get; set; }
    }
}
