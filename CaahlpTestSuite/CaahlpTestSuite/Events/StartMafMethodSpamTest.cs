﻿using CAALHP.Events;

namespace TestSuite.Events
{
    public class StartMafMethodSpamTest : Event
    {
        public int Cycles { get; set; }
    }
}
