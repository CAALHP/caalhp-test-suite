﻿using CAALHP.Events;

namespace TestSuite.Events
{
    public class TestMafResponseEvent : Event
    {
        public int Index { get; set; }
    }
}
