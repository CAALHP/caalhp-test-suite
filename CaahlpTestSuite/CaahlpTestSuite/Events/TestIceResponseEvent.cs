﻿using CAALHP.Events;

namespace TestSuite.Events
{
    public class TestIceResponseEvent : Event
    {
        public int Index { get; set; }
    }
}
