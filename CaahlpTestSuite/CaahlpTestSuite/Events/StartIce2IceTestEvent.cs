﻿using CAALHP.Events;

namespace TestSuite.Events
{
    public class StartIce2IceTestEvent : Event
    {
        public int Cycles { get; set; }
    }
}
