﻿using CAALHP.Events;

namespace TestSuite.Events
{
    public class TestIceRequestEvent : Event
    {
        public int Index { get; set; }
    }
}
