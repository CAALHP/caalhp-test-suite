﻿using CAALHP.Events;

namespace TestSuite.Events
{
    public class TestCompletedEvent : Event
    {
        public string Name { get; set; }
        public int Cycles { get; set; }
        public long DurationInTicks { get; set; }
        public long AverageTimePerEvent { get; set; }
    }
}
