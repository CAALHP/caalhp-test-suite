﻿using CAALHP.Events;

namespace TestSuite.Events
{
    public class TestMafRequestEvent : Event
    {
        public int Index { get; set; }
    }
}
