﻿using CAALHP.Events;

namespace TestSuite.Events
{
    public class StartMaf2IceTestEvent : Event
    {
        public int Cycles { get; set; }
    }
}
